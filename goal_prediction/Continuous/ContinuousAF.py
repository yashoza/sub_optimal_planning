import numpy as np

from matplotlib import pyplot
from shapely.geometry import MultiPolygon,Point
from descartes.patch import PolygonPatch
from shapely.geometry.polygon import Polygon
from shapely.ops import cascaded_union


class ContinuousAF:
    def __init__(self, limits = [0,100,0,100], obstacles = []):
        """obstacles is a list of point coordinates (NOT numpy arrays),
        obstacle_sides is a list of segments (i.e., a list of two points
        on the perimeter of an obstacle, and points is a list of all the
        points in the world.
        Every listing of points is done CLOCKWISE."""
        
        self.obstacles = obstacles
        
        self.xmin = limits[0]
        self.xmax = limits[1]
        self.ymin = limits[2]
        self.ymax = limits[3]
        
        # Add world limits
        self.obstacles.append([[self.xmin,self.ymin], [self.xmin,self.ymax], [self.xmax,self.ymax], [self.xmax,self.ymin], [self.xmin,self.ymin]])
        
        # proper lists (easier to check if a given element is a member)
        self.obstacle_sides = []
        self.points = []
        for obs in obstacles:
            if not obs[0] in self.points:
                self.points.append(obs[0])
            for i in range(1,len(obs)):
                self.obstacle_sides.append([obs[i-1], obs[i]])
                if not obs[i] in self.points:
                    self.points.append(obs[i])
    
    
    def findCollision(self, pt1, pt2, pt3, pt4):
        """returns the collision point between segment 1, joining points pt1
        and pt2, and segment 2, joining points pt3 and pt4.
        If no collision point exists, return np.nan."""
        
        # convert everything into np.arrays
        pt1 = np.array(pt1)
        pt2 = np.array(pt2)
        pt3 = np.array(pt3)
        pt4 = np.array(pt4)
        
        vec1 = pt2 - pt1
        vec2 = pt4 - pt3
        
        if np.linalg.norm(vec1) > 0:
            dir1 = vec1 / np.linalg.norm(vec1)
        else:
            # pt1 and pt2 are the same
            # check if pt1 lies on the segment [pt3;pt4]
            if np.isclose(np.dot(pt1-pt3,pt1-pt4),np.linalg.norm(pt1-pt3)*np.linalg.norm(pt1-pt4)) \
            and np.linalg.norm(pt1-pt3) <= np.linalg.norm(vec2) \
            and np.linalg.norm(pt1-pt4) <= np.linalg.norm(vec2):
                return pt1
            else:
                return np.nan
        if np.linalg.norm(vec2) > 0:
            dir2 = vec2 / np.linalg.norm(vec2)
        else:
            # pt3 and pt4 are the same
            # check if pt3 lies on the segment [pt1;pt2]
            if np.isclose(np.dot(pt3-pt1,pt3-pt2),np.linalg.norm(pt3-pt1)*np.linalg.norm(pt3-pt2)) \
            and np.linalg.norm(pt3-pt1) <= np.linalg.norm(vec1) \
            and np.linalg.norm(pt3-pt2) <= np.linalg.norm(vec1):
                return pt3
            else:
                return np.nan
        
        # Check if the lines are parallel
        if np.isclose(np.abs(np.dot(dir1,dir2)),1.0):
            # check if both points are on the same line
            if np.isclose(pt1,pt2).all():
                # pt1 and pt2 are the same, return either
                return pt1
            
            if np.isclose(np.dot(pt3-pt1,pt3-pt2),np.linalg.norm(pt3-pt1)*np.linalg.norm(pt3-pt2)) \
            and np.linalg.norm(pt3-pt1) <= np.linalg.norm(vec1) \
            and np.linalg.norm(pt3-pt2) <= np.linalg.norm(vec1):
                return pt3
            elif np.isclose(np.dot(pt4-pt1,pt4-pt2),np.linalg.norm(pt4-pt1)*np.linalg.norm(pt4-pt2)) \
            and np.linalg.norm(pt4-pt1) <= np.linalg.norm(vec1) \
            and np.linalg.norm(pt4-pt2) <= np.linalg.norm(vec1):
                return pt4
            else:
                return np.nan
        
        # vec1 is not parallel to vec2, our life is easier
        b = pt1 - pt3
        A = np.array([[-vec1[0],vec2[0]],[-vec1[1],vec2[1]]])
        t = np.linalg.solve(A, b)
        if (0.<=t[0]<=1. or np.isclose(t[0],0.) or np.isclose(t[0],1.)) and \
           (0.<=t[1]<=1. or np.isclose(t[1],0.) or np.isclose(t[1],1.)):
            return pt1 + t[0]*vec1
        return np.nan
    
    
    def findObstNumber(self, pt):
        """return the id of the obstacle pt belongs to.
        If pt does not belong to any obstacle, returns nan."""
        
        if pt not in self.points:
            return np.nan
        
        for i in range(len(self.obstacles)):
            if pt in self.obstacles[i]:
                return i
        
        return np.nan
    
    
    def interiorEdgeQ(self, pt1, pt2):
        """return True if the edge connecting vertices of the same obstacle
        passes by the interior of the obstacle, and False in any other case."""
        
        if np.isnan(np.array([pt1,pt2])).any():
            return True
        
        # array cast for next lines
        pt1 = list(pt1)
        pt2 = list(pt2)
        
        # find the obstacle edge[0] belongs to
        obstNum = self.findObstNumber(pt1)
        if np.isnan(obstNum):
            return False
        obst = self.obstacles[obstNum]
        
        # find neighbors
        nVert = len(obst)-1
        pt1ID = obst.index(pt1)
        lneigh = (pt1ID - 1) % nVert
        rneigh = (pt1ID + 1) % nVert
        
        # define vectors entering the edge
        pt1 = np.array(pt1)
        pt2 = np.array(pt2)
        lpt = np.array(obst[lneigh])
        rpt = np.array(obst[rneigh])
        vec1 = lpt - pt1
        vec2 = rpt - pt1
        
        if np.isclose(np.dot(vec1,vec2),np.linalg.norm(vec1)*np.linalg.norm(vec2)):
            # vec1 and vec2 are collinear...
            return True
        
        # find linear combination of vec1 and vec2
        b = pt2 - pt1 # edge to check
        A = np.array([[vec1[0],vec2[0]],[vec1[1],vec2[1]]])
        t = np.linalg.solve(A, b)
        
        # check if the solution is a set of two strictly positive numbers
        result = False
        if t[0] > 0 and t[1] > 0:
            result = True
        
        # Check if the corner is concave: if so, invert solution -> Useless apparently...
        cornAngle = (np.arctan2(vec1[1],vec1[0]) - np.arctan2(vec2[1],vec2[0])) % (2*np.pi)
        if cornAngle < np.pi:
            result = not result
        
        # invert result if pt1 lies on the world border
        if obstNum == len(self.obstacles)-1:
            result = not result
        
        if np.isclose(pt2,lpt).all() or np.isclose(pt2,rpt).all() or np.isclose(pt2,pt1).all():
            result = False
        
        return result
    
    
    def projectPoint(self, pt, vec, segment):
        """returns the intersection of the line defined by the
        position vector pt, and the direction vector vec, onto
        the segment segment, if it exists. Otherwise, returns nan."""
        
        cases = [[self.xmin,pt[0],vec[0],pt[1],vec[1],self.ymin,self.ymax,1], [self.xmax,pt[0],vec[0],pt[1],vec[1],self.ymin,self.ymax,1], \
                 [self.ymin,pt[1],vec[1],pt[0],vec[0],self.xmin,self.xmax,0], [self.ymax,pt[1],vec[1],pt[0],vec[0],self.xmin,self.xmax,0]]
        
        pt2 = np.nan
        
        # project ray onto the world border, i.e., try each border segment
        for c in cases:
            if c[2] != 0:
                lam = (c[0] - c[1]) / c[2]
                y = c[3] + lam*c[4]
                if lam > 0 and c[5] <= y <= c[6]:
                    pt2 = [c[0],c[0]]
                    pt2[c[7]] = y
                    break
        
        # check for each border point if the ray [pt,borderPt] intersects segment
        colPt = self.findCollision(pt, pt2, segment[0], segment[1])
        if not np.isnan(colPt).any():
            return colPt
        
        # otherwise, return nan
        return np.nan
    
    
    def freeEdgeQ(self, pt1, pt2):
        """return True if the edge is acceptable (i.e., no
        collision with an obstacle), otherwise returns False.
        A collision at any end of the segment (i.e., pt1 or pt2
        lying on the edge of an obstacle, doesn't count as a collision."""
        
        if self.interiorEdgeQ(pt1,pt2):
            return False
        
        for segment in self.obstacle_sides:
            colPt = self.findCollision(pt1, pt2, segment[0], segment[1])
            if np.isnan(colPt).any() or np.isclose(colPt,pt1).all() or np.isclose(colPt,pt2).all():
                continue
            elif list(colPt) in self.points and self.freeEdgeQ(colPt,pt2):
                continue
            else:
                return False
        
        return True
    
    
    def visiblePoints(self, pt):
        """returns a list of all the points visible from point pt.
        The list contains at least point pt."""
        
        vispt = []
        
        for obspt in self.points:
            if self.freeEdgeQ(pt,obspt):
                vispt.append(obspt)
        
        return [np.array(item) for item in vispt]
    
    
    def sortVisCell(self, visPts, pt):
        """sorts the visibility Cell array created in visibilityCell()."""
        
        closerPts = sorted(visPts, key=lambda item: np.sqrt((item[0]-pt[0])**2 + (item[1]-pt[1])**2)) # sort by distance to pt
        ptFull = closerPts[0]
        closerPts.remove(ptFull)
        
        obstNum = self.findObstNumber(ptFull[:2])
        nVert = len(self.obstacles[obstNum])-1
        idPt = self.obstacles[obstNum].index(ptFull[:2])
        neighb0 = self.obstacles[obstNum][(idPt-1) % nVert]
        
        # and find its angle from the considered point pt
        if np.isclose(np.array(pt[:2]),np.array(ptFull[:2])).all():
            angle0 = np.arctan2(neighb0[1]-pt[1],neighb0[0]-pt[0])
        else:
            angle0 = np.arctan2(ptFull[1]-pt[1],ptFull[0]-pt[0])
        
        # now let's sort every point by their angle relative to angle0
        visCell = sorted(closerPts, key=lambda item: ((np.arctan2(item[1]-pt[1],item[0]-pt[0])-angle0+np.pi) % (2*np.pi) - np.pi) % (2*np.pi)) # sort counter-clockwise
        
        # nice homemade sort...
        # sorts each group of same-angled points into sublists, and sorts these by radius
        tmpCell = []
        angle = 0
        tmp = [ptFull] # add the first item at the front, since it should be there but was moved by "-angle0"
        for point in visCell:
            aPt = ((np.arctan2(point[1]-pt[1],point[0]-pt[0])-angle0+np.pi) % (2*np.pi) - np.pi) % (2*np.pi)
            if np.isclose(aPt,angle):
                tmp.append(point)
            else:
                tmpSort = sorted(tmp, key=lambda item: (item[0]-pt[0])**2 + (item[1]-pt[1])**2)
                tmpCell.append(tmpSort)
                tmp = [point]
                angle = aPt
        tmpSort = sorted(tmp, key=lambda item: (item[0]-pt[0])**2 + (item[1]-pt[1])**2)
        tmpCell.append(tmpSort)
        
        # and now the magic begins
        # tries to sort create a sensible list of points, and if fails
        # reverts the first sublist and tries again...
        workedQ = False
        for retry in range(2):
            if not workedQ:
                workedQ = True
                for i in range(1,len(tmpCell)):
                    prevLast = tmpCell[i-1][-1]
                    thisFirst = tmpCell[i][0]
                    thisLast = tmpCell[i][-1]
                    
                    if retry == 0 and prevLast[2] != thisFirst[2] and prevLast[2] != thisLast[2]:
                        workedQ = False
                        tmpCell[0] = tmpCell[0][::-1]
                        break
                    
                    if thisFirst[2] != prevLast[2]:
                        tmpCell[i] = tmpCell[i][::-1]
        
        return [item for sublist in tmpCell for item in sublist]
    
    
    def visibilityCell(self, pt):
        """returns the set of vertices defining the perimeter of
        the visibility cell of the point pt."""
        
        visPts = self.visiblePoints(pt)
        visPts = [list(item) for item in visPts]
        
        for i in range(len(visPts)):
            visPt = visPts[i]
            visPts[i] = visPt + [self.findObstNumber(visPt)]
            # try and project the point further onto an other obstacle
            for obsEdge in self.obstacle_sides:
                colPt = self.projectPoint(pt, np.array(visPt)-np.array(pt), obsEdge)
                if not np.isnan(colPt).any():
                    if self.freeEdgeQ(visPt,colPt): # free edge, let's add it if not already present
                        addQ = True
                        for v in visPts:
                            if np.isclose(np.array(v[:2]),colPt).all():
                                addQ = False
                                break
                        if addQ:
                            visPts.append(list(colPt)+[self.findObstNumber(obsEdge[0])])
        
        sortedCell = self.sortVisCell(visPts,pt)
        
        return sortedCell
    
    
    def reconstruct_path(self, start, goal, prevNode):
        """reconstruct the optimal path from start to goal
        from the prevNode dictionary, giving a list of
        successive points from start to goal"""
        
        path = [goal]
        currNode = goal
        while currNode != start:
            currNode = prevNode[tuple(currNode)]
            path = [currNode] + path
        
        return path
    
    
    def pathCost(self, path):
        """returns the cost of following the given path."""
        
        if len(path) < 2:
            return 0.
        
        cost = 0.
        for i in range(1,len(path)):
            cost += np.sqrt( (path[i][0]-path[i-1][0])**2 + (path[i][1]-path[i-1][1])**2 )
        
        return cost
    
    
    def astar(self, start, goal):
        """returns the optimal path from start to goal in the world"""
        
        openList = [ start ]
        closedList = []
        fvalues = { tuple(start): 0. }
        gvalues = { tuple(start): np.sqrt( (start[0]-goal[0])**2 + (start[1]-goal[1])**2 ) }
        prevNode = {}
        
        self.points.append(goal)
        borderCopy = self.obstacles[-1][:]
        for i in range(4):
            self.points.remove(self.obstacles[-1][i])
        
        while len(openList) > 0:
            currNode = openList[0]
            openList.remove(currNode)
            closedList.append(currNode)
            
            if currNode == goal:
                self.points.remove(goal)
                for i in range(4):
                    self.points.append(borderCopy[i])
                return self.reconstruct_path(start, goal, prevNode)
            
            for vp in self.visiblePoints(currNode):
                vp = list(vp)
                if vp in closedList:
                    continue
                gval = gvalues[tuple(currNode)] + np.sqrt( (currNode[0]-vp[0])**2 + (currNode[1]-vp[1])**2 )
                
                # only consider points not on the border
                if vp[0] >= self.xmax-1 or vp[0] <= self.xmin+1 or vp[1] >= self.ymax-1 or vp[1] <= self.ymin+1:
                    continue
                
                if vp not in openList:
                    openList.append(vp)
                elif gval >= gvalues[tuple(vp)]:
                    continue
                
                prevNode[tuple(vp)] = currNode
                gvalues[tuple(vp)] = gval
                fvalues[tuple(vp)] = gval + np.sqrt( (vp[0]-goal[0])**2 + (vp[1]-goal[1])**2 )
            
            openList = sorted(openList, key=lambda item: fvalues[tuple(item)])
        
        self.points.remove(goal)
        for i in range(4):
            self.points.append(borderCopy[i])
        return np.nan
    
    
    def drawLocus(self, start, goals, bound, colors, figNum=1):
        """draw the locus of bounded suboptimal trajectories from
        start to goals, knowing the suboptimality bound (1 + x%)"""
        
        # try every edge in the system
        points = [start] + goals + self.points
        loci = []
        
        borderPts = []
        for pt in points:
            if pt[0] >= self.xmax-1 or pt[0] <= self.xmin+1 or pt[1] >= self.ymax-1 or pt[1] <= self.ymin+1:
                borderPts.append(pt)
        for bpt in borderPts:
            points.remove(bpt)
        
        print "Initializing..."
        
        polyCells = []
        pathsStart = []
        for i in range(len(points)):
            polyCells.append( Polygon(self.visibilityCell(points[i])) )
            pathsStart.append(self.astar(points[i],start))
        
        for g in range(len(goals)):
            print "Finding suboptimal trajectories to goal", g+1
            goal = goals[g]
            
            pathsGoal = []
            for i in range(len(points)):
                pathsGoal.append(self.astar(points[i],goal))
            
            optimalPath = pathsGoal[0]
            assert(not np.isnan(optimalPath).any())
            
            optimalCost = self.pathCost(optimalPath)
            #maxCost = optimalCost * bound
            maxCost = optimalCost + bound # switched to absolute bound
            
            patches = []
            for i in range(len(points)):
                pt1 = points[i]
                if np.sqrt( (pt1[0]-start[0])**2 + (pt1[1]-start[1])**2 ) + np.sqrt( (pt1[0]-goal[0])**2 + (pt1[1]-goal[1])**2 ) <= maxCost: # simple euclidean pruning
                    path1s = pathsStart[i]
                    path1g = pathsGoal[i]
                    vis1 = polyCells[i]
                    for j in range(i+1,len(points)):
                        pt2 = points[j]
                        if np.sqrt( (pt2[0]-start[0])**2 + (pt2[1]-start[1])**2 ) + np.sqrt( (pt2[0]-goal[0])**2 + (pt2[1]-goal[1])**2 ) <= maxCost: # simple euclidean pruning
                            path12 = self.astar(pt1,pt2)
                            path2s = pathsStart[j]
                            path2g = pathsGoal[j]
                            if not (np.isnan(path12 + path1s + path1g + path2s + path2g)).any():
                                cost12 = self.pathCost(path12)
                                costRest1 = self.pathCost(path1s) + self.pathCost(path2g)
                                costRest2 = self.pathCost(path2s) + self.pathCost(path1g)
                                currCost = cost12 + costRest1
                                if costRest2 < costRest1:
                                    currCost = cost12 + costRest2
                                surPlus = maxCost - currCost
                                if surPlus > 0:
                                    length = cost12 + surPlus
                                    vis2 = polyCells[j]
                                    ell = Polygon(self.getEllipsePoints(pt1[0],pt1[1],pt2[0],pt2[1],length))
                                    try:
                                        vis12 = vis1.intersection(vis2)
                                        patches.append(vis12.intersection(ell))
                                    except:
                                        continue
            
            print "Cascaded_union on the Polygons"
            loci.append(cascaded_union(patches))
            
        print "Plotting result"
        self.drawPatches(loci,start,goals,colors,figNum)
    
    
    def drawPatches(self, patches, start, goals, colors, figNum=1):
        """draws a list of Polygons or MultiPolygons, as generated by
        the drawLocus() function. Also plots the start/goals points and
        the obstacles"""
        
        self.figure = pyplot.figure(figNum, (100,100), dpi=90)
        self.handle = self.figure.add_subplot(111)
        self.handle.set_xlim([self.xmin+1,self.xmax-1])
        self.handle.set_ylim([self.ymin+1,self.ymax-1])
        self.handle.set_aspect(1)
        
        for i in range(len(self.obstacles)-1):
            self.handle.add_patch(PolygonPatch(Polygon(self.obstacles[i]), fc='#000000'))
        
        for i in range(len(patches)):
            if patches[i].geom_type == "Polygon":
                self.handle.add_patch(PolygonPatch(patches[i], fc=colors[i], ec=colors[i], alpha=0.5, zorder=0.5))
            elif patches[i].geom_type == "MultiPolygon":
                for poly in patches[i]:
                    self.handle.add_patch(PolygonPatch(poly, fc=colors[i], ec=colors[i], alpha=0.5, zorder=0.5))
        
        # start/goal points (add on top)
        self.handle.plot(start[0], start[1], 'o', color='#ff3333', zorder=1)
        for i in range(len(goals)):
            self.handle.plot(goals[i][0], goals[i][1], 'o', color=colors[i], zorder=1)
        
        pyplot.show()
    
    
    def plotCell(self, point, figNum=1):
        """plot visibility cell on the figure handle"""
        
        visCell = self.visibilityCell(point)
        
        cellPolygon = Polygon(visCell)
        #cellPatch = PolygonPatch(cellPolygon)
        
        self.drawPatches([[cellPolygon]], point, [point], ['#6699cc'], figNum=figNum)
    
    
    def getEllipsePoints(self, xf1, yf1, xf2, yf2, length, nvals=100):
        """returns a set of 1000 points defining the perimeter of an
        ellipse around the focal points (xf1,yf1) and (xf2,yf2), with
        largest axis 2*a = length. Used to draw an ellipse using Shapely."""
        
        d = np.sqrt( (xf2-xf1)**2 + (yf2-yf1)**2 )
        cx = (xf1 + xf2) / 2
        cy = (yf1 + yf2) / 2
        
        a = 0.5 * length
        b = 0.5 * np.sqrt( length**2 - d**2 )
        
        w = np.arctan2(yf2-yf1,xf2-xf1)
        x1 = cx + a * np.cos(w)
        y1 = cy + a * np.sin(w)
        x2 = cx - a * np.cos(w)
        y2 = cy - a * np.sin(w)
        
        t = np.linspace(0,2*np.pi,nvals)
        X = a * np.cos(t)
        Y = b * np.sin(t)
        
        x = list((x1+x2)/2 + X*np.cos(w) - Y*np.sin(w))
        y = list((y1+y2)/2 + X*np.sin(w) + Y*np.cos(w))
        
        return [[x[i],y[i]] for i in range(nvals)]
