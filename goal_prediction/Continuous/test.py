import numpy as np
import ContinuousAF as CAF

obs = [ [[10., 190.], [50., 190.], [50., 150.], [10., 150.], [10., 190.]], \
        [[61., 170.], [111., 170.], [111., 160.], [61., 160.], [61., 170.]], \
        [[10., 130.], [40., 130.], [40., 30.], [10., 30.], [10., 130.]], \
        [[70., 140.], [90., 140.], [90., 20.], [70., 20.], [70., 140.]], \
        [[130., 200.], [160., 200.], [160., 110.], [130., 110.], [130., 200.]], \
        [[100., 100.], [160., 100.], [160., 40.], [100., 40.], [100., 100.]] ]
limits = [-1,201,-1,201]
start = [5.,185.]
goals = [ [180.,30.],[80.,10.],[180.,170.] ]

bound = 5.
colors = ['#6699cc', '#cc6699', '#99cc66']

caf = CAF.ContinuousAF(obstacles = obs, limits=limits)

#print caf.drawLocus(start,[goals[0]],bound.,1)
#print caf.drawLocus(start,[goals[1]],bound.,2)
#print caf.drawLocus(start,[goals[2]],bound.,3)

caf.drawLocus(start, goals, bound, colors)
