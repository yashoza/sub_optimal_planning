function ellipsesMaps = plotEllipses(inflationFactor, nGoals)

if nargin < 2
    nGoals = 1;
end
if nargin < 1
    inflationFactor = 1;
end

tic; system(sprintf('python infAstar.py %.2f',inflationFactor)); toc

[sizeY,sizeX] = size(load('ellipses0B.mat','-ascii'));
ellipsesMaps = zeros(sizeY,sizeX,3,nGoals);

for g = 1:nGoals
    ellipsesMaps(:,:,1,g) = load(sprintf('ellipses%dR.mat',g-1),'-ascii');
    ellipsesMaps(:,:,2,g) = load(sprintf('ellipses%dG.mat',g-1),'-ascii');
    ellipsesMaps(:,:,3,g) = load(sprintf('ellipses%dB.mat',g-1),'-ascii');
    
    subplottight(ceil(nGoals/3),3,g), imshow(ellipsesMaps(:,:,:,g));
end

figure(2)
clf
imshow(mean(ellipsesMaps,4), 'border', 'tight');
