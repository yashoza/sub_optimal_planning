import Astar_Policy, sys
from numpy import array

FUDGEFACTOR = .01

class EllipsesMap(object):
    def __init__(self, world_map):
	self.r = []
	self.g = []
	self.b = []
	self.sizeX = len(world_map)
	self.sizeY = len(world_map[0])
	
	for i in range(sizeX):
	    self.r.append(list(1-array(world_map[i])))
	    self.g.append(self.r[i][:])
	    self.b.append(self.r[i][:])
    
    
    def inMap(self, coord):
	# True if within bounds, False otherwise
	return (0 <= coord[0] < self.sizeX) and (0 <= coord[1] < sizeY)
    
    
    def plotCross(self, coord, color, size=1):
	if self.inMap(coord):
	    self.r[coord[0]][coord[1]] = color[0]
	    self.g[coord[0]][coord[1]] = color[1]
	    self.b[coord[0]][coord[1]] = color[2]
	
	for s in range(1,size+1):
	    for c in ((coord[0]+s,coord[1]+s),(coord[0]+s,coord[1]-s),(coord[0]-s,coord[1]+s),(coord[0]-s,coord[1]-s)):
		if self.inMap(c):
		    self.r[c[0]][c[1]] = color[0]
		    self.g[c[0]][c[1]] = color[1]
		    self.b[c[0]][c[1]] = color[2]
    
    
    def toString(self, map="all"):
	string = ""
	
	if map == "r" or map == "all":
	    for i in range(sizeX):
		for j in range(sizeY-1):
		    string += str(self.r[i][j]) + " "
		string += str(self.r[i][sizeY-1]) + "\n"
	if map == "g" or map == "all":
	    for i in range(sizeX):
		for j in range(sizeY-1):
		    string += str(self.g[i][j]) + " "
		string += str(self.g[i][sizeY-1]) + "\n"
	if map == "b" or map == "all":
	    for i in range(sizeX):
		for j in range(sizeY-1):
		    string += str(self.b[i][j]) + " "
		string += str(self.b[i][sizeY-1]) + "\n"
	
	return string
    
    
    def saveMaps(self, filename):
	ofp = open(filename+"R.mat","w")
	ofp.write(self.toString("r"))
	ofp.close()
	
	ofp = open(filename+"G.mat","w")
	ofp.write(self.toString("g"))
	ofp.close()
	
	ofp = open(filename+"B.mat","w")
	ofp.write(self.toString("b"))
	ofp.close()


def createWorld(sizeY, sizeX, obstacles=[]):
    # obstacles should be a tuple of (x0,y0,obsX,obsY) for each obstacle
    # returns a tuple-typed world map with 0 (free space), 1 (obstacles)
    world_map = [[0,]*sizeY]*sizeX
    
    if obstacles != []:
	world_map = list(world_map) # list transfo to change it
	for (x0,y0,obsX,obsY) in obstacles:
	    for i in range(y0,y0+obsY):
		line = list(world_map[i])
		line[x0:x0+obsX] = [1,]*obsX
		world_map[i] = tuple(line)
    
    return tuple(world_map)


# loading world_map
#ifp = open("world.txt","r")
#world_def = ifp.readline()
#world_map = eval(world_def)
world_map = createWorld(200,200,((10,10,40,40),(100,100,60,60),(60,30,50,12),(10,70,30,100),(70,60,20,120),(130,0,10,90)))
sizeX = len(world_map)
sizeY = len(world_map[0])
inflationFactor = 1.
if len(sys.argv) > 1:
    inflationFactor = float(sys.argv[1])
goals = ((195,195),(180,130),(30,180))
init = (15,5)

initColor = (.5,.5,.5)
goalColors = ((1,1,0),(0,1,1),(1,0,1))
colors = ((0.95,0,0),(0,0.95,0),(0,0,0.95))

# test infAstar planner
forwardPlanners = []
ellipsesMaps = []
for goal in goals:
    forwardPlanners.append(Astar_Policy.Astar_Graph(world_map, goal, connect_8=True, diagonal_cost=True, makespan=True))
    ellipsesMaps.append(EllipsesMap(world_map))
backwardPlanner = Astar_Policy.Astar_Graph(world_map, init, connect_8=True, diagonal_cost=True, makespan=True)

for g in range(len(goals)):
    maxCost = inflationFactor + forwardPlanners[g].get_cost(init)

    for i in range(sizeX):
	for j in range(sizeY):
	    if world_map[i][j] == 0:
		forwardCost = forwardPlanners[g].get_cost((i,j))
		if forwardCost <= maxCost: # some small pruning
		    if (forwardCost + backwardPlanner.get_cost((i,j))) <= maxCost+FUDGEFACTOR:
			ellipsesMaps[g].r[i][j] = colors[g][0]
			ellipsesMaps[g].g[i][j] = colors[g][1]
			ellipsesMaps[g].b[i][j] = colors[g][2]
    
    # Mark init/goal positions in their respective colors
    ellipsesMaps[g].plotCross(init, initColor, int(round(sizeX/100)))
    ellipsesMaps[g].plotCross(goals[g], goalColors[g], int(round(sizeX/100)))
    
    # save mat files
    ellipsesMaps[g].saveMaps("ellipses%d" % (g))
